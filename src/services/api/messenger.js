import http from "axios";

const API_URL = "/jq_messenger.php";

const timeoutMessageUpdate = 5000;
let timeoutMessageUpdateId = null;

const timeoutDialogsUpdate = 5000;
let timeoutDialogsUpdateId = null;

export const getDialogByAccid = async (cb = null) => {
  const { data } = await http.get(API_URL, {
    params: {
      a: "getrooms",
    },
  });

  if (cb) cb(data);

  return data;
};

export const getDialogById = async (mmrId, target) => {
  const { data } = await http.get(API_URL, {
    params: {
      a: "getroom",
      mmrId,
      target,
    },
  });

  return data;
};

export const subscribeDialogs = async (cb) => {
  await getDialogByAccid(cb);

  timeoutDialogsUpdateId = setInterval(() => {
    getDialogByAccid(cb);
  }, timeoutDialogsUpdate);
};

export const unsubscribeDialogs = () => {
  clearInterval(timeoutDialogsUpdateId);
};

const getMessages = async (mmrId, target, cb) => {
  const { data } = await http.get(API_URL, {
    params: {
      a: "getmessages",
      mmrId,
      target,
    },
  });

  cb(data);
};

export const subscribeOnRoomMessages = async (mmrId, target, cb) => {
  await getMessages(mmrId, target, cb);

  timeoutMessageUpdateId = setInterval(() => {
    getMessages(mmrId, target, cb);
  }, timeoutMessageUpdate);
};

export const unsubscribeOnRoomMessages = () => {
  clearInterval(timeoutMessageUpdateId);
};

export const pushMessage = async (mmrId, target, message, file = null) => {
  const { data } = await http.post(
    API_URL,
    {
      file,
    },
    {
      params: {
        a: "sendmessage",
        mmrId,
        message,
        target,
      },
    }
  );

  return data;
};

export const messageRead = async (mmrId, messageId) => {
  const { data } = await http.post(
    API_URL,
    {
      mmrId,
      messageId,
    },
    {
      params: {
        a: "messageread",
      },
    }
  );

  return data;
};

export const searchDialogs = async (search) => {
  const { data } = await http.get(API_URL, {
    params: {
      a: "searchrooms",
      search,
    },
  });

  return data;
};
