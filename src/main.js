import Vue from "vue";
import App from "./App.vue";
import store from "./store";

import "./scss/app.scss";
import "./quasar";

import VueRouter from "vue-router";
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
});

Vue.config.productionTip = false;

export function init(el) {
  // both of these are idempotent - no need to run them only once
  Vue.config.productionTip = false;

  return new Vue({ store, router, el, render: (h) => h(App) });
}

new Vue({ el: "#app", router, store, render: (h) => h(App) });
